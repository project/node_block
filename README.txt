Create any number of custom blocks and automatically 
associate them for display on a particular (node) page view. 

Useful for granting non-admin users the ability to insert 
custom blocks

Block display region is set in admin/settings/node_block.

Saves the trouble of going to configure the block manually
in admin/block

On deleting the block association, the block itself is deleted from 
the block table.

No database tables are created by this module.